#include <memory>

class Interface
{
public:
    virtual ~Interface() = default;
    virtual void doSomething() const = 0;
};

// separate base class for "cloneable" objects
class cloneable
{
public:
    virtual ~cloneable() = default;

    std::unique_ptr<cloneable> clone() const
    {
        return std::unique_ptr<cloneable>(this->clone_impl());
    }

private:
    virtual cloneable* clone_impl() const = 0;
};

// NOTE: alternatively, Interface could inherit from cloneable (if we wanted to allow making copies using the Interface)
class A : public Interface, public cloneable
{
public:
    void doSomething() const override
    {
        // do some work here...
    }

    // Possible solution for the problem with covariance: define this method
    // (but it is useless duplication)
    /*
    std::unique_ptr<A> clone() const
    {
        return std::unique_ptr<A>(this->clone_impl());
    }
    */

private:
    virtual A* clone_impl() const override
    {
        return new A(*this);
    }
};

int main()
{
    // make an object
    A obj;
    obj.doSomething();

    // make a copy
    auto copy = obj.clone();

    // PROBLEM: this does not work - copy has type std::unique_ptr<cloneable>, not std::unique_ptr<A>
    copy->doSomething();
}