class Interface
{
public:
    virtual ~Interface() = default;
    virtual void doSomething() const = 0;
};

class A : public Interface
{
public:
    void doSomething() const override
    {
        // do some work here...
    }
};

void f(const A& x)
{
    // here it is easy to make a copy
    A y = x;
}

void g(const Interface& x)
{
    // how to make a copy using the Interface?
    Interface y = x; // ERROR: Interface is an abstract class
}

int main()
{
    A obj;
    f(obj);
    g(obj);
}