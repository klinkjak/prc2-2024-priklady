class Interface
{
public:
    virtual ~Interface() = default;
    virtual void doSomething() const = 0;

    // new method in the interface
    virtual Interface* clone() const = 0;
};

class A : public Interface
{
public:
    void doSomething() const override
    {
        // do some work here...
    }

    // NOTE: the return type is `A*` instead of `Interface*`
    // (it is possible for pointers thanks to covariance)
    A* clone() const override
    {
        return new A(*this);
    }
};

void f(const A& x)
{
    // here it is easy to make a copy
    A y = x;
}

void g(const Interface& x)
{
    // here we can make a copy using the clone() method
    Interface* y = x.clone();

    // do some work here...

    // PROBLEM: we must know and remember to call delete
    delete y;
}

int main()
{
    A obj;
    f(obj);
    g(obj);
}