#include "tuple.h"
#include <iostream>

int main()
{
    my::tuple<int> t1;
    my::tuple<int, double> t2;
    my::tuple<int, double, int> t3;

    my::tuple<int, double, int> t4(1, 2.3, 4);

    int a0 = my::get<0>(t4);
    double a1 = my::get<1>(t4);
    int a2 = my::get<2>(t4);
    std::cout << a0 << ", " << a1 << ", " << a2 << std::endl;
}