#pragma once

namespace my {

// dopredna deklarace
template< int I, class... Types >
class storage;

// obecna specializace
template< int I, typename Head, class... Tail >
class storage<I, Head, Tail...>
: public storage<I + 1, Tail...>  // rekurze
{
private:
    Head value;

public:
    storage() = default;

    storage(Head arg0, Tail... args)
    : value(arg0), storage<I + 1, Tail...>(args...)
    {}

    template<int Index>
    auto get()
    {
        // pocet prvku v "Tail" resp. "args"
        // sizeof...(Tail)  resp.  sizeof...(args)

        if constexpr (I == Index)
            return value;
        else
            return storage<I + 1, Tail...>::template get<Index>();
    }
};

// specializace pro ukonceni rekurze
template <int I>
class storage<I>
{};

template< class... Types >
class tuple
: public storage<0, Types...>
{
public:
    tuple() = default;

    tuple(Types... args)
    : storage<0, Types...>(args...)
    {}

    template<int Index>
    auto get()
    {
        return storage<0, Types...>::template get<Index>();
    }
};

template<int Index, typename... Types>
auto
get(tuple<Types...> t)
{
    return t.template get<Index>();
}

} // namespace my