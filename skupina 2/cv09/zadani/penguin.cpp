class Bird
{
public:
    virtual void setLocation(double longitude, double latitude) = 0;
    virtual void setAltitude(double altitude) = 0;
    virtual void draw() = 0;
};

class Parrot : public Bird
{
private:
    double longitude = 0;
    double latitude = 0;
    double altitude = 0;

public:
    virtual void setLocation(double longitude, double latitude)
    {
        this->longitude = longitude;
        this->latitude = latitude;
    }

    virtual void setAltitude(double altitude)
    {
        this->altitude = altitude;
    }

    virtual void draw()
    {
        // zde by byla nějaká užitečná implementace (nejspíš pomocí nějaké grafické knihovny)...
    }
};

class Penguin : public Bird
{
private:
    double longitude = 0;
    double latitude = 0;

public:
    virtual void setLocation(double longitude, double latitude)
    {
        this->longitude = longitude;
        this->latitude = latitude;
    }

    virtual void setAltitude(double altitude)
    {
        // Penguin does not fly - what to do here???
    }

    virtual void draw()
    {
        // zde by byla nějaká užitečná implementace (nejspíš pomocí nějaké grafické knihovny)...
    }
};