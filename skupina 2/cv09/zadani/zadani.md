## Úkol

Opravte programy tak, aby neporušovaly [Liskov Substitution Principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle) ani [Interface Segregation Principle](https://en.wikipedia.org/wiki/Interface_segregation_principle).

1. `square.cpp` – typická ukázka porušení LSP, podrobnější popis viz [Wikipedia](https://en.wikipedia.org/wiki/Circle%E2%80%93ellipse_problem)
2. `penguin.cpp` – typická ukázka porušení ISP
3. `IncreasingSet.cpp` – Pokud se uživatel pokusí použít funkci `f` s objektem typu `IncreasingSet`, jde o neplatný program a mělo by dojít k chybě při kompilaci. Je potřeba změnit/rozšířit/rozdělit rozhraní abstraktní třídy `Set`.
