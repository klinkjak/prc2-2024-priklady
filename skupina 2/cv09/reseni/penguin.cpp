#include <stdexcept>

class Bird
{
public:
    virtual void setLocation(double longitude, double latitude) = 0;
    virtual void draw() = 0;
};

class FlyingBird : public Bird
{
public:
    virtual void setAltitude(double altitude) = 0;
};

class Parrot : public FlyingBird
{
private:
    double longitude = 0;
    double latitude = 0;
    double altitude = 0;

public:
    virtual void setLocation(double longitude, double latitude)
    {
        this->longitude = longitude;
        this->latitude = latitude;
    }

    virtual void setAltitude(double altitude)
    {
        this->altitude = altitude;
    }

    virtual void draw()
    {
        // zde by byla nějaká užitečná implementace (nejspíš pomocí nějaké grafické knihovny)...
    }
};

class Penguin : public Bird
{
private:
    double longitude = 0;
    double latitude = 0;

public:
    virtual void setLocation(double longitude, double latitude)
    {
        this->longitude = longitude;
        this->latitude = latitude;
    }

    /*
    virtual void setAltitude(double altitude)
    {
        // Penguin does not fly - what to do here???
        // 1. do nothing...
        // 2. throw an exception
        throw std::logic_error("Penguins don't fly!");
    }
    */

    virtual void draw()
    {
        // zde by byla nějaká užitečná implementace (nejspíš pomocí nějaké grafické knihovny)...
    }
};

/*
void adjustPositionOnGround(Bird& bird);
void adjustPositionInSky(Bird& bird);

// general function: should check if the bird can fly
void adjustPosition(Bird& bird)
{
    // 1. přidat getAltitude -> Penguin by vyvolal vyjimku
    // 2. dynamic_cast
    Penguin* p = dynamic_cast<Penguin*>(&bird);
    if (p == nullptr)
        adjustPositionInSky(bird); // bird is Parrot
    else
        adjustPositionOnGround(bird); // bird is Penguin
    // 3. přidat metodu canFly()
}
*/

void adjustPosition(Bird& bird);
void adjustPosition(FlyingBird& bird);