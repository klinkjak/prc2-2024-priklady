/**
 * Abstraktní třída definující rozhraní pro množinu prvků.
 *
 * (Rozhraní je jen pro ilustraci, není úplné...)
 */
template <typename T, typename Iterator>
class Set
{
public:
    virtual void add(const T&) = 0;
    virtual bool contains(const T&) const = 0;
    virtual Iterator begin() const = 0;
    virtual Iterator end() const = 0;
};

template <typename T, typename Iterator>
class RemovableSet : public Set<T, Iterator>
{
public:
    virtual void remove(const T&) = 0;
};

/**
 * Konkrétní implementace množiny pomocí externí knihovny - STL.
 *
 * (Opět pouze pro ilustraci...)
 */
#include <unordered_set>

template <typename T>
class UnorderedSet
    : public RemovableSet<T, typename std::unordered_set<T>::const_iterator>
{
private:
    using Iterator = typename std::unordered_set<T>::const_iterator;
    std::unordered_set<T> data;

public:
    void add(const T& v) override { data.insert(v); }
    void remove(const T& v) override { data.erase(v); }
    bool contains(const T& v) const override { return data.count(v) > 0; }
    Iterator begin() const override { return data.begin(); }
    Iterator end() const override { return data.end(); }
};

/**
 * Implementace množiny, ze které nelze odebírat prvky.
 */
template <typename T>
class IncreasingSet
    : public Set<T, typename std::unordered_set<T>::const_iterator>
{
private:
    using Iterator = typename std::unordered_set<T>::const_iterator;
    std::unordered_set<T> data;

public:
    void add(const T& v) override { data.insert(v); }
    bool contains(const T& v) const override { return data.count(v) > 0; }
    Iterator begin() const override { return data.begin(); }
    Iterator end() const override { return data.end(); }
};

/**
 * Implementace nějaké funkce pracující s množinou (pouze pro ilustraci).
 */
#include <iostream>
template <typename T, typename Iterator>
void f(RemovableSet<T, Iterator>& s)
{
    if (!s.contains(0)) {
        s.add(1);
        s.remove(2);
    }

    for (Iterator it = s.begin(); it != s.end(); it++)
        std::cout << *it << std::endl;
}

int main()
{
    // zde není problém
    UnorderedSet<int> set;
    f(set);

    // zde je problém - funkce f nefunguje s proměnnou typu IncreasingSet
    IncreasingSet<int> incSet;
    f(incSet);
}
