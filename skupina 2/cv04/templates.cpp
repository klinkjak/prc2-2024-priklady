#if 0
int max(int a, int b)
{
    return (a < b) ? b : a;
}

long max(long a, long b)
{
    return (a < b) ? b : a;
}

float max(float a, float b)
{
    return (a < b) ? b : a;
}

double max(double a, double b)
{
    return (a < b) ? b : a;
}
#endif

#include <iostream>
#include <cstring>

template <typename T1, typename T2>
//auto max(T1 a, T2 b)
std::common_type_t<T1, T2> max(T1 a, T2 b)
{
    static_assert(!std::is_pointer_v<T1>, "you cannot use pointers!!!");
    static_assert(!std::is_pointer_v<T2>, "you cannot use pointers!!!");
    return (a < b) ? b : a;
    //if (a < b)
    //    return b;
    //else
    //    return a;
}

// specializace pro stringy
//template <>
const char* max(const char* a, const char* b)
{
    return (std::strcmp(a, b) > 0) ? a : b;
}

// zobecněná varianta pro libovolný počet parametrů
template <typename T, typename... Ts>
auto max(T a1, Ts... args)
{
    /* možné operace s "parameter packem":
        1. sizeof...(args) -> počet typů/argumentů v packu
        2. f(args...) -> předání hodnot jiné funkci
    */
    //return max(/* args[0] */, max(/* args[1:] */))
    return max(a1, max(args...));
}

int main()
{
    int m1 = max(1, -1);
    float m2 = max(1.0, 3.14);
    double m3 = max(1, 3.14);
    double m4 = max(3.14, 1);
    char m5 = max('a', 'z');
    const char* m6 = max("z", "a");

    int m7 = max(0, 1, 2, 3, 4, 5);

    std::cout << m1 << std::endl;
    std::cout << m2 << std::endl;
    std::cout << m3 << std::endl;
    std::cout << m4 << std::endl;
    std::cout << m5 << std::endl;
    std::cout << m6 << std::endl;
}