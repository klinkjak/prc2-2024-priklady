#ifndef POINTS_H
#define POINTS_H

// obecná deklarace šablonové třídy
template <int D, typename T>
class Point;

// specializace třídy pro D == 2
template <typename T>
class Point<2, T>
{
private:
    T x;
    T y;

public:
    Point(T x, T y);

    // gettery
    //T getX() const { return x; }
    //T getY() const { return y; }

    // settery
    void setX(T x);// { this->x = x; }
    void setY(T y);// { this->y = y; }

    // get-settery
    T& getX();// { return x; }
    T& getY();// { return y; }

    const T& getX() const;// { return x; }
    const T& getY() const;// { return y; }
};

// specializace šablonové třídy pro D == 3
template <typename T>
class Point<3, T>
{
private:
    T x;
    T y;
    T z;

public:
    Point(T x, T y, T z);
    //: x(x), y(y), z(z)
    //{}

    // gettery
    //T getX() const { return x; }
    //T getY() const { return y; }
    //T getZ() const { return z; }

    // settery
    void setX(T x);// { this->x = x; }
    void setY(T y);// { this->y = y; }
    void setZ(T z);// { this->z = z; }

    // get-settery
    T& getX();// { return x; }
    T& getY();// { return y; }
    T& getZ();// { return z; }

    const T& getX() const;// { return x; }
    const T& getY() const;// { return y; }
    const T& getZ() const;// { return z; }
};

#include "points.hpp"

#endif