#pragma once

// 3 distance functions: Eukleides, Manhattan, maximum

#include "points.h"

template <typename T>
double distanceEukleides(const Point2D<T>& p1,
                         const Point2D<T>& p2);

template <typename T>
double distanceEukleides(const Point3D<T>& p1,
                         const Point3D<T>& p2);

#include "distance.hpp"