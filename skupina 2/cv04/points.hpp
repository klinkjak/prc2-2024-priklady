#pragma once

#include "points.h"

template <typename T>
Point2D<T>::Point2D(T x, T y)
: x(x), y(y)
{}

// settery
template <typename T>
void Point2D<T>::setX(T x) { this->x = x; }
template <typename T>
void Point2D<T>::setY(T y) { this->y = y; }

// get-settery
template <typename T>
T& Point2D<T>::getX() { return x; }
template <typename T>
T& Point2D<T>::getY() { return y; }

template <typename T>
const T& Point2D<T>::getX() const { return x; }
template <typename T>
const T& Point2D<T>::getY() const { return y; }


template <typename T>
Point3D<T>::Point3D(T x, T y, T z)
: x(x), y(y), z(z)
{}

// settery
template <typename T>
void Point3D<T>::setX(T x) { this->x = x; }
template <typename T>
void Point3D<T>::setY(T y) { this->y = y; }
template <typename T>
void Point3D<T>::setZ(T z) { this->z = z; }

// get-settery
template <typename T>
T& Point3D<T>::getX() { return x; }
template <typename T>
T& Point3D<T>::getY() { return y; }
template <typename T>
T& Point3D<T>::getZ() { return z; }

template <typename T>
const T& Point3D<T>::getX() const { return x; }
template <typename T>
const T& Point3D<T>::getY() const { return y; }
template <typename T>
const T& Point3D<T>::getZ() const { return z; }