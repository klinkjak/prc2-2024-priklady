#pragma once

#include "distance.h"

#include <cmath>

template <typename T>
double distanceEukleides(const Point2D<T>& p1,
                         const Point2D<T>& p2)
{
    T dx = p1.getX() - p2.getX();
    T dy = p1.getY() - p2.getY();
    return std::sqrt( dx * dx + dy * dy );
    //return std::sqrt( std::pow(p1.getX() - p2.getX(), 2)
    //                  + std::pow(p1.getY() - p2.getY(), 2) );
}

template <typename T>
double distanceEukleides(const Point3D<T>& p1,
                         const Point3D<T>& p2)
{
    T dx = p1.getX() - p2.getX();
    T dy = p1.getY() - p2.getY();
    T dz = p1.getZ() - p2.getZ();
    return std::sqrt( dx * dx + dy * dy + dz * dz );
}