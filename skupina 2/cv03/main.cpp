// 2 classes: Point2D, Point3D
// 3 distance functions: Eukleides, Manhattan, maximum
//
// classes for objects: circle, square, sphere, cube
// function  bool isInside(Object, Point)

#include "points.h"
#include "distance.h"
#include <iostream>

int main()
{
    Point2D a(1, 1);
    Point2D b(2, 3);

    double d = distanceEukleides(a, b);
    std::cout << d << std::endl;
}