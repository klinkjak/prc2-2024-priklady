#include "distance.h"

#include <cmath>

double distanceEukleides(const Point2D& p1, const Point2D& p2)
{
    double dx = p1.getX() - p2.getX();
    double dy = p1.getY() - p2.getY();
    return std::sqrt( dx * dx + dy * dy );
    //return std::sqrt( std::pow(p1.getX() - p2.getX(), 2)
    //                  + std::pow(p1.getY() - p2.getY(), 2) );
}

double distanceEukleides(const Point3D& p1, const Point3D& p2)
{
    double dx = p1.getX() - p2.getX();
    double dy = p1.getY() - p2.getY();
    double dz = p1.getZ() - p2.getZ();
    return std::sqrt( dx * dx + dy * dy + dz * dz );
}