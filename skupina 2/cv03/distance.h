#pragma once

// 3 distance functions: Eukleides, Manhattan, maximum

#include "points.h"

double distanceEukleides(const Point2D& p1, const Point2D& p2);

double distanceEukleides(const Point3D& p1, const Point3D& p2);