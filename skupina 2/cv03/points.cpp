#include "points.h"

Point2D::Point2D(double x, double y)
: x(x), y(y)
{}

// settery
void Point2D::setX(double x) { this->x = x; }
void Point2D::setY(double y) { this->y = y; }

// get-settery
double& Point2D::getX() { return x; }
double& Point2D::getY() { return y; }

const double& Point2D::getX() const { return x; }
const double& Point2D::getY() const { return y; }


Point3D::Point3D(double x, double y, double z)
: x(x), y(y), z(z)
{}

// settery
void Point3D::setX(double x) { this->x = x; }
void Point3D::setY(double y) { this->y = y; }
void Point3D::setZ(double z) { this->z = z; }

// get-settery
double& Point3D::getX() { return x; }
double& Point3D::getY() { return y; }
double& Point3D::getZ() { return z; }

const double& Point3D::getX() const { return x; }
const double& Point3D::getY() const { return y; }
const double& Point3D::getZ() const { return z; }