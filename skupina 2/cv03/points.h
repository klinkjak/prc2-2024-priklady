#ifndef POINTS_H
#define POINTS_H

// 2 classes: Point2D, Point3D

class Point2D
{
private:
    double x;
    double y;

public:
    Point2D(double x, double y);

    // gettery
    //double getX() const { return x; }
    //double getY() const { return y; }

    // settery
    void setX(double x);// { this->x = x; }
    void setY(double y);// { this->y = y; }

    // get-settery
    double& getX();// { return x; }
    double& getY();// { return y; }

    const double& getX() const;// { return x; }
    const double& getY() const;// { return y; }
};

class Point3D
{
private:
    double x;
    double y;
    double z;

public:
    Point3D(double x, double y, double z);
    //: x(x), y(y), z(z)
    //{}

    // gettery
    //double getX() const { return x; }
    //double getY() const { return y; }
    //double getZ() const { return z; }

    // settery
    void setX(double x);// { this->x = x; }
    void setY(double y);// { this->y = y; }
    void setZ(double z);// { this->z = z; }

    // get-settery
    double& getX();// { return x; }
    double& getY();// { return y; }
    double& getZ();// { return z; }

    const double& getX() const;// { return x; }
    const double& getY() const;// { return y; }
    const double& getZ() const;// { return z; }
};

#endif