#include <fstream>
#include <iostream>
#include <string>

std::string read_word(std::fstream& input)
{
    std::string word;
    input >> word;
    return word;
}

void text_statistics(std::fstream& input)
{
    // Ukoly:
    // 1. spocitat pocet slov
    // 2. najit nejdelsi slovo
    // 3. najit nejkratsi slovo
    // 4. najit 5 nejcastejsich pismen
    // Nakonec vsechno vypsat.
    int word_count = 0;
    std::string longest_word;
    std::string shortest_word;

#if 0
    for (;;) {
        // precteni nasledujiciho slova
        std::string word;
        input >> word;

        // kontrola chyb
        /*
        if (input.eof()) {
            break;
        }
        else if (input.fail()) {
            std::cout << "error: failed to read word from file" << std::endl;
            break;
        }
        */
        // jednodussi kontrola:
        if (!input.good()) {
            break;
        }

        // reseni ukolu
        word_count++;
    }
#endif
    //for (std::string word = read_word(input); input.good(); input >> word) {
    std::string word;
    for (input >> word; input.good(); input >> word) {
        // reseni ukolu
        word_count++;

        if (word.length() > longest_word.length()) {
            longest_word = word;
        }
        if (shortest_word.empty() || word.length() < shortest_word.length()) {
            shortest_word = word;
        }
    }

    std::cout << "word count is " << word_count << std::endl;
    std::cout << "longest word: " << longest_word << std::endl;
    std::cout << "shortest word: " << shortest_word << std::endl;
}

int main()
{
    std::fstream file("vstup.txt", std::ios::in);

    // spusteni algoritmu
    text_statistics(file);
}