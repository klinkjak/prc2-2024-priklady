#include <fstream>
#include <iostream>
#include <string>

int main()
{
    std::fstream file("vstup.txt", std::ios::in);

    // cteni dat:
    char c = file.get();  // precte jeden znak

    // precteni celeho radku
    std::string s;
    std::getline(file, s);
    std::cout << "Radek je: " << s << std::endl;

    // precteni jednoho slova
    file >> s;
    std::cout << "dalsi slovo: " << s << std::endl;
    file >> s;
    std::cout << "dalsi slovo: " << s << std::endl;
    file >> s;
    std::cout << "dalsi slovo: " << s << std::endl;

    // precteni cisla
    int a;
    file >> a;
    if (file.fail()) {
        // doslo k chybe cteni (bud chyba v programu nebo v OS nebo v HW)
        std::cout << "something failed" << std::endl;
        return 1;
    }

    std::getline(file, s);
    std::cout << "Radek je: " << s << std::endl;
}