#include <iostream>
#include <cmath>

double odmocnina(double x, double epsilon = 1e-4)
{
    if (x < 0) {
        return NAN;
    }

    // obecny pripad: puleni intervalu (0, x)
    double a = 0;
    double b = x;
    double c;

    // specialni pripad: puleni intervalu (x, 1)
    if (x < 1) {
        a = x;
        b = 1;
    }

    do {
        // rozpuleni intervalu
        c = (a + b) / 2;

        //if (c == NAN) ... vzdy false
        //if (isnan(c)) ... spravny postup

        // spocitani druhe mocniny
        double c_squared = c * c;

        // porovnani se vstupni hodnotou
        if (c_squared < x - epsilon) {
            // posun dolni meze intervalu
            a = c;
        }
        else if (c_squared > x + epsilon) {
            // posun horni meze intervalu
            b = c;
        }
        else {
            break;
        }
    } while (true);
    
    return c;
}

int main()
{
    double x = 9;
    double y = odmocnina(x);
    std::cout << "odmocnina(" << x << ") = " << y << std::endl;
}