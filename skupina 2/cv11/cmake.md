# CMake

[CMake](https://cmake.org/) je multiplatformní nástroj pro automatizování kompilace, testování, balíčkování a instalaci software.
CMake je nezávislý na překladači a vývojovém prostředí programátora.

CMake sám o sobě není _build system_, ale _generátor konfigurace_ pro build systémy jako Make, Ninja, apod.
Překlad projektu pomocí CMake tedy probíhá ve dvou fázích:

1. Konfigurace CMake a generování konfigurace pro build system.
   V terminálu se provádí typicky příkazem

   ```sh
   cmake -B build -S . -G Ninja
   ```

   - parametr `-B` určuje adresář, kde bude probíhat překlad (typicky `build`)
   - parametr `-S` určuje cestu k top-level adresáři projektu (`.` odkazuje na aktuální adresář)
   - volitelný parametr `-G` určuje typ generátoru

2. Kompilování projektu, případně generování dalších souborů (dokumentace, obrázky, atd.).
   V terminálu se provádí typicky příkazem

   ```sh
   cmake --build build
   ```

   Cesta `build` za parametrem `--build` určuje adresář, kde má probíhat překlad (odpovídá parametru `-B` v předchozím příkladu).

## Konfigurace projektu

Konfigurace se provádí v souborech `CMakeLists.txt` umístěných mezi zdrojovými soubory v projektu.
Nejjednodušší nastavení projektu může vypadat takto:

```cmake
cmake_minimum_required(VERSION 3.10)
project(MyAwesomeProject)
add_executable(my-target my-source.cpp)
```

Další užitečná makra:

- Nastavení standardu C++:
    ```cmake
    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
    ```
- Nastavení parametrů překladače (GCC, Clang):
    ```cmake
    set(CMAKE_CXX_FLAGS "-Wall -pedantic")
    set(CMAKE_CXX_FLAGS_DEBUG "-g")
    set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
    ```
- Přidání podadresáře obsahujícího soubor `CMakeLists.txt`:
    ```cmake
    add_subdirectory(src)
    ```
- Přidání knihovny k binárnímu souboru – viz [target_include_directories](https://cmake.org/cmake/help/latest/command/target_include_directories.html) a [target_link_libraries](https://cmake.org/cmake/help/latest/command/target_link_libraries.html).

Více informací najdete v [oficiálním tutoriálu](https://cmake.org/cmake/help/latest/guide/tutorial/index.html),
na webu [Modern CMake](https://cliutils.gitlab.io/modern-cmake/)
nebo v projektu [CMake Examples](https://github.com/ttroy50/cmake-examples).

## Šablony

Šablony s počáteční konfigurací CMake pro C++ projekty:

- https://github.com/TheLartians/ModernCppStarter
- https://github.com/filipdutescu/modern-cpp-template
- https://github.com/cpp-best-practices/cmake_template
