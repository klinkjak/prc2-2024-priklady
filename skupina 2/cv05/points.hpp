#pragma once

#include "points.h"

template <int D, typename T>
Point<D, T>::Point(T x)
: data{x}
{
    // vyvoláme chybu, když D != 1
    static_assert(D == 1, "tento konstruktor lze pouzit jen kdyz dimenze je 1");
}

template <int D, typename T>
Point<D, T>::Point(T x, T y)
: data{x, y}
{
    // vyvoláme chybu, když D != 2
    static_assert(D == 2, "tento konstruktor lze pouzit jen kdyz dimenze je 2");
}

template <int D, typename T>
Point<D, T>::Point(T x, T y, T z)
: data{x, y, z}
{
    // vyvoláme chybu, když D != 3
    static_assert(D == 3, "tento konstruktor lze pouzit jen kdyz dimenze je 3");
}


// settery
template <int D, typename T>
void Point<D, T>::setX(T x)
{
    data[0] = x;
}

template <int D, typename T>
void Point<D, T>::setY(T y)
{
    static_assert(D >= 2);
    data[1] = y;
}

template <int D, typename T>
void Point<D, T>::setZ(T z)
{
    static_assert(D >= 3);
    data[2] = z;
}


// get-settery
template <int D, typename T>
T& Point<D, T>::getX()
{
    return data[0];
}

template <int D, typename T>
T& Point<D, T>::getY()
{
    static_assert(D >= 2);
    return data[1];
}

template <int D, typename T>
T& Point<D, T>::getZ()
{
    static_assert(D >= 3);
    return data[2];
}

template <int D, typename T>
const T& Point<D, T>::getX() const
{
    return data[0];
}

template <int D, typename T>
const T& Point<D, T>::getY() const
{
    static_assert(D >= 2);
    return data[1];
}

template <int D, typename T>
const T& Point<D, T>::getZ() const
{
    static_assert(D >= 3);
    return data[2];
}


template <int D, typename T>
int Point<D, T>::getDimension() const
{
    return D;
}

template <int D, typename T>
T& Point<D, T>::operator[](int i)
{
    //if (i >= D)
    //    vyvolat vyjimku;
    return data[i];
}

template <int D, typename T>
const T& Point<D, T>::operator[](int i) const
{
    //if (i >= D)
    //    vyvolat vyjimku;
    return data[i];
}


template <int D, typename T>
bool Point<D, T>::operator<(const Point& other) const
{
    
}