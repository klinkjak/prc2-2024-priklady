#pragma once

// 3 distance functions: Eukleides, Manhattan, maximum

#include "points.h"

template <int D, typename T>
double distanceEukleides(const Point<D, T>& p1,
                         const Point<D, T>& p2);

#include "distance.hpp"