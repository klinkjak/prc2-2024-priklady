#ifndef POINTS_H
#define POINTS_H

// obecná deklarace šablonové třídy
template <int D, typename T>
class Point
{
    static_assert(D >= 1);

private:
    // statické pole s daty
    T data[D];

public:
    // konstruktory
    Point();
    Point(T x);
    Point(T x, T y);
    Point(T x, T y, T z);

    // settery
    void setX(T x);
    void setY(T y);
    void setZ(T z);

    // get-settery
    T& getX();
    T& getY();
    T& getZ();

    const T& getX() const;
    const T& getY() const;
    const T& getZ() const;

    // další metody pro iterování
    int getDimension() const;

    T& operator[](int i);
    const T& operator[](int i) const;

    // pro použití v std::set
    bool operator<(const Point& other) const;
};

#include "points.hpp"

#endif