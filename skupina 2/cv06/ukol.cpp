#include <functional>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <string>

#include "distance.h"
#include "points.h"

template <int dim, typename T>
class PointCloud
{
public:
    using Point = ::Point<dim, T>;
    using CallbackFunction = std::function<void(Point)>;

    // defaultní konstruktor
    PointCloud() = default;

    // konstruktor s callback funkcí (volá se při přidání nového bodu)
    PointCloud(CallbackFunction callback)
    : callback(callback)
    {}

    int getDimension() const
    {
        return dim;
    }

    void addPoint(const Point& p)
    {
        points.insert(p);
        if (callback)
            callback(p);
    }

    // metoda pro nalezení dvou nejvzdálenějších bodů (úkoly 1 a 2)
    // MetricFunction - typ funkce pro měření vzdálenosti; použití: d = dist(p1, p2);
    template <typename MetricFunction>
    std::pair<Point, Point> findMostDistantPoints(MetricFunction dist) const;

private:
    CallbackFunction callback = nullptr;
    std::set<Point> points;
};


template <int dim, typename T>
template <typename MetricFunction>
// return type:
//std::pair<typename PointCloud<dim, T>::Point, typename PointCloud<dim, T>::Point>
//std::pair<Point<dim, T>, Point<dim, T>>
auto
// class name + method name + ...
PointCloud<dim, T>::findMostDistantPoints(MetricFunction dist) const
// return type:
-> std::pair<Point, Point>
{
    double max_distance = -1;
    Point a;
    Point b;

    for (auto& p1 : points) {
        for (auto& p2 : points) {
            auto d = dist(p1, p2);
            if (d > max_distance) {
                max_distance = d;
                a = p1;
                b = p2;
            }
        }
    }

    //return std::pair<Point, Point>(a, b);
    return {a, b};
}

/*
definice funkce:

R f() {}    <=>     auto f() -> R {}

*/


template <typename Cloud>
void readPoints(Cloud& cloud, std::istream& str)
{
    while (true) {
        std::string line;
        std::getline(str, line);
        if (!str)
            break;

        std::stringstream s(line);
        typename Cloud::Point p;
        //for (auto& component : p)
        //    s >> component;
        for (int i = 0; i < p.getDimension(); i++)
            s >> p[i];

        if (s.eof() && !s.fail())
            cloud.addPoint(p);
        else
            std::cerr << "chyba pri cteni dat z radku \"" << line << "\"" << std::endl;
    }
}

int main()
{
    auto callback = [](const auto& p)
    {
        std::cout << "pridan novy bod" << std::endl;
    };

    constexpr int dim = 3;
    //PointCloud<2> cloud;
    PointCloud<dim, double> cloud(callback);

    std::ifstream data;
    if (dim == 2)
        data.open("data_2d.txt");
    if (dim == 3)
        data.open("data_3d.txt");
    readPoints(cloud, data);

    // Úkol 1: najděte 2 nejvzdálenější body dle maximové metriky
    //auto p = cloud.findMostDistantPoints(...);

    // Úkol 2: najděte 2 nejvzdálenější body dle Eukleidovské metriky
    //double (*metrika)(const Point<2, double>&, const Point<2, double>&) = distanceEukleides<2, double>;
    auto metrika = distanceEukleides<dim, double>;
    auto p = cloud.findMostDistantPoints(metrika);

    // Úkol 3: modifikujte celý program pro 3D (souřadnice x,y,z viz soubor data_3d.txt)

    // vypis vysledku
    //std::cout << "[" << p.first[0] << ", " << p.first[1] << "]" << std::endl;
    //std::cout << "[" << p.second[0] << ", " << p.second[1] << "]" << std::endl;
    // přetížený operátor
    std::cout << p.first << std::endl;
    std::cout << p.second << std::endl;
}
