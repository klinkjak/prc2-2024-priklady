#pragma once

#include "PointCloud.h"

template <int dim, typename T, typename MetricFunction>
PointCloud<dim, T>
filterNearPoints(const PointCloud<dim, T>& cloud,
                 const Point<dim, T>& center,
                 T radius,
                 MetricFunction metric)
{
    PointCloud<dim, T> pc;

    for (const auto& i : cloud) {
        auto d = metric(center, i);
        if (d <= radius)
            pc.addPoint(i);
    }

    return pc;
}

template <int dim, typename T, typename MetricFunction>
PointCloud<dim, T>
filterPointsBetween(const PointCloud<dim, T>& cloud,
                    const Point<dim, T>& center,
                    T r1,
                    T r2,
                    MetricFunction metric)
{
    PointCloud<dim, T> pc;

    for (const auto& i : cloud) {
        auto d = metric(center, i);
        if (r1 <= d && d <= r2)
            pc.addPoint(i);
    }

    return pc;
}

template <int dim, typename T, typename MetricFunction>
PointCloud<dim, T>
filterPointsNotBetween(const PointCloud<dim, T>& cloud,
                       const Point<dim, T>& center,
                       T r1,
                       T r2,
                       MetricFunction metric)
{
    PointCloud<dim, T> pc;

    for (const auto& i : cloud) {
        auto d = metric(center, i);
        if (r1 >= d || d >= r2)
            pc.addPoint(i);
    }

    return pc;
}


// obecné řešení 1: "specification" je instance nějaké třídy
template <int dim, typename T, typename FilterSpecification>
PointCloud<dim, T>
filter(const PointCloud<dim, T>& cloud,
       FilterSpecification specification)
{
    PointCloud<dim, T> pc;

    for (const auto& i : cloud) {
        if (specification.is_satisfied(i))
            pc.addPoint(i);
    }

    return pc;
}

// obecné řešení 2: "specification" je funkce
template <int dim, typename T, typename FilterSpecification>
PointCloud<dim, T>
functional_filter(const PointCloud<dim, T>& cloud,
                  FilterSpecification specification)
{
    PointCloud<dim, T> pc;

    for (const auto& i : cloud) {
        if (specification(i))
            pc.addPoint(i);
    }

    return pc;
}