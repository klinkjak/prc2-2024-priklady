#include "distance.h"
#include "PointCloud.h"
#include "filters.h"

int main()
{
    auto callback = [](const auto& p)
    {
        std::cout << "pridan novy bod" << std::endl;
    };

    constexpr int dim = 3;
    PointCloud<dim, double> cloud(callback);

    std::ifstream data;
    if (dim == 2)
        data.open("data_2d.txt");
    if (dim == 3)
        data.open("data_3d.txt");
    readPoints(cloud, data);

    auto metrika = distanceEukleides<dim, double>;
    Point<dim, double> point(1, 2, 3);
    double r = 4;

    // Úkol 1: filtrování bodů z PointCloudu
    // input: cloud, bod, metrika, poloměr
    // output: podmnožina bodů z cloudu

    // 1. přístup: "prototypování"
    // přímo zde by byla celá implementace algoritmu

    // 2. přístup: samostatná metoda ve třídě PointCloud
    //auto result = cloud.filterNearPoints(point, r, metrika);

    // 3. přístup: samostatná funkce
    auto result2 = filterNearPoints(cloud, point, r, metrika);



    // Úkol 2: filtrování bodů z PointCloudu
    // input: cloud, bod, metrika, poloměry r1 a r2
    // output: podmnožina bodů z cloudu: r1 <= dist <= r2
    auto result3 = filterPointsBetween(cloud, point, r, 2*r, metrika);

    // Úkol 3: filtrování bodů z PointCloudu
    // input: cloud, bod, metrika, poloměry r1 a r2
    // output: podmnožina bodů z cloudu: dist <= r1 OR r2 <= dist
    auto result4 = filterPointsNotBetween(cloud, point, r, 2*r, metrika);


    // Obecné řešení 1: pro bounding-box
    Point<dim, double> p1(1, 2, 3);
    Point<dim, double> p2(2, 4, 6);
    BoundingBoxSpecification specBoundingBox(p1, p2);

    BoundingBoxSpecification differentBoundingBox(p1, p2);

    // objektový bonus
    auto spec = specBoundingBox && differentBoundingBox;
}
