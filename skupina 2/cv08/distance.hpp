#pragma once

#include "distance.h"

#include <cmath>

template <int D, typename T>
double distanceEukleides(const Point<D, T>& p1,
                         const Point<D, T>& p2)
{
    T s = 0;
    for (int i = 0; i < p1.getDimension(); i++) {
        T delta = p1[i] - p2[i];
        s += delta * delta;
    }
    return std::sqrt(s);
}