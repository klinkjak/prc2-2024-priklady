#include <iostream>

int main()
{
    int b = 1;
    int c = 2;

    //auto f = [&](int a)
    auto f = [b, &c](int a)
    {
        std::cout << "Hello world!" << std::endl;
        std::cout << a << std::endl;
        std::cout << b << std::endl;
        std::cout << c << std::endl;
        c = a;
    };

    f(42);
    c = 3;
    f(42);
    std::cout << "c = " << c << std::endl;
}