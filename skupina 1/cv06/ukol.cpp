#include <functional>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <string>

#include "points.h"
#include "distance.h"

template <int dim, typename T>
class PointCloud
{
public:
    using Point = ::Point<dim, T>;
    using CallbackFunction = std::function<void(Point)>;

    // defaultní konstruktor
    PointCloud() = default;

    // konstruktor s callback funkcí (volá se při přidání nového bodu)
    PointCloud(CallbackFunction callback)
    : callback(callback)
    {}

    int getDimension() const
    {
        return dim;
    }

    void addPoint(const Point& p)
    {
        points.insert(p);
        if (callback)
            callback(p);
    }

    // metoda pro nalezení dvou nejvzdálenějších bodů (úkoly 1 a 2)
    // MetricFunction - typ funkce pro měření vzdálenosti; použití: d = dist(p1, p2);
    template <typename MetricFunction>
    std::pair<Point, Point> findMostDistantPoints(MetricFunction dist) const
    {
        T max_distance = -1;
        Point a;
        Point b;

        //for (auto p1 = points.begin(); p1 != points.end(); p1++)
        for (auto p1 : points) {
            for (auto p2 : points) {
                T d = dist(p1, p2);
                if (max_distance < d) {
                    max_distance = d;
                    a = p1;
                    b = p2;
                }
            }
        }

        //return std::make_pair(a, b);
        return {a, b};
    }

private:
    CallbackFunction callback = nullptr;
    std::set<Point> points;
};

template <typename Cloud>
void readPoints(Cloud& cloud, std::istream& str)
{
    while (true) {
        std::string line;
        std::getline(str, line);
        if (!str)
            break;

        std::stringstream s(line);
        typename Cloud::Point p;
        //for (auto& component : p)
        //    s >> component;
        for (int i = 0; i < p.getDimension(); i++)
            s >> p[i];

        if (s.eof() && !s.fail())
            cloud.addPoint(p);
        else
            std::cerr << "chyba pri cteni dat z radku \"" << line << "\"" << std::endl;
    }
}

int main()
{
#if 0
    Point<2, double> p(1.0, 2.0);
    p[0] = 3.0;
    std::cout << p.getX() << std::endl;
#else
    auto callback = [](const auto& p)
    {
        std::cout << "pridan novy bod" << std::endl;
    };

    constexpr int dim = 3;
    //PointCloud<dim> cloud;
    PointCloud<dim, double> cloud(callback);

    std::ifstream data;
    if (dim == 2)
        data.open("data_2d.txt");
    else if (dim == 3)
        data.open("data_3d.txt");
    readPoints(cloud, data);

    // Úkol 1: najděte 2 nejvzdálenější body dle maximové metriky
    //double (*metrika) (const Point<dim, double>&, const Point<dim, double>&) = ChebyshevDistance<dim, double, double>;
    auto metrika = ChebyshevDistance<dim, double, double>;
    auto p = cloud.findMostDistantPoints(metrika);

    // Úkol 2: najděte 2 nejvzdálenější body dle Eukleidovské metriky
    //double (*metrika2) (const Point<dim, double>&, const Point<dim, double>&) = EukleideanDistance<dim, double, double>;
    auto metrika2 = EukleideanDistance<dim, double, double>;
    auto p2 = cloud.findMostDistantPoints(metrika2);

    // Úkol 3: modifikujte celý program pro 3D (souřadnice x,y,z viz soubor data_3d.txt)

    // vypis vysledku
    std::cout << "vysledek 1:" << std::endl;
    //std::cout << "[" << p.first.getX() << ", " << p.first.getY() << "]" << std::endl;
    //std::cout << "[" << p.second.getX() << ", " << p.second.getY() << "]" << std::endl;
    // TODO: přetížit další operátor
    std::cout << p.first << std::endl;
    std::cout << p.second << std::endl;
    
    std::cout << "vysledek 2:" << std::endl;
    //std::cout << "[" << p2.first.getX() << ", " << p2.first.getY() << "]" << std::endl;
    //std::cout << "[" << p2.second.getX() << ", " << p2.second.getY() << "]" << std::endl;
    std::cout << p2.first << std::endl;
    std::cout << p2.second << std::endl;
#endif
}
