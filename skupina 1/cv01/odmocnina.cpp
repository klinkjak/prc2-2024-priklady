#include <iostream>
#include <cmath>

double odmocnina(double x, double epsilon = 1e-6)
{
    if (x < 0) {
        std::cerr << "odmocnina záporného čisla nejde spočítat" << std::endl;
        return NAN;
    }
    
    // obecný případ - půlení intervalu (0, x)
    double a = 0;
    double b = x;

    // ošetření speciálního případu - půlení intervalu (x, 1)
    if (x < 1) {
        a = x;
        b = 1;
    }

    double c;
    while (true) {
        //c = a + (b - a) / 2;
        c = (a + b) / 2;
        double c_square = c * c;
        if (c_square < x - epsilon) {
            a = c;
        }
        else if(c_square > x + epsilon) {
            b = c;
        }
        else {
            break;
        }
    }
    return c;
}

int main()
{
    double x = 0.5;
    double y = odmocnina(x);
    std::cout << "odmocnina(" << x << ") = " << y << std::endl;
}