#include <iostream>
#include <fstream>
#include <string>

int main()
{
    // otevreme soubor pro cteni
    std::fstream file("vstup.txt", std::ios::in);

    // cteni dat ze souboru
    std::string s;
    file >> s;  // precte jedno slovo
    std::cout << "Prvni slovo je " << s << std::endl;
    file >> s;  // precte jedno slovo
    std::cout << "Druhe slovo je " << s << std::endl;

    // cteni ciselnych hodnot
    int a;
    file >> a;  // zde nastane chyba - dalsi operace se nepovedou!
    // kontrola chyb
    if (!file.good()) {
        std::cout << "nastala chyba" << std::endl;
        return 1;
    }

    // cteni celeho radku
    std::getline(file, s);
    std::cout << "Zbytek prvniho radku: " << s << std::endl;

    // cteni jednoho znaku
    char c = file.get();
}