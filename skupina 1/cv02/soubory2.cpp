#include <iostream>
#include <fstream>
#include <string>

void text_statistics(std::fstream& input)
{
    // Ukoly:
    // 1. najit a vypsat nejdelsi slovo
    // 2. najit a vypsat nejkratsi slovo
    // 3. 

    std::string longest_word;
    std::string shortest_word;

    // dokud nedojdeme na konec souboru, tak opakujeme
    while (!input.eof()) {
        // precteme slovo
        std::string word;
        input >> word;

        // zkontrolovat chyby
        if (input.bad()) {
            // tohle nastane napr. pri chybe systemu nebo disku
            std::cout << "pri cteni souboru nastala nejaka chyba" << std::endl;
            return;
        }
        else if (input.fail()) {
            break; // tohle nastane napr. kdyz jsou na konci souboru bile znaky
        }

        if (word.length() > longest_word.length()) {
            longest_word = word;
        }
        if (shortest_word.empty() || word.length() < shortest_word.length()) {
            shortest_word = word;
        }
    }

    std::cout << "Longest word: " << longest_word << std::endl;
    std::cout << "Shortest word: " << shortest_word << std::endl;
}

int main()
{
    // otevreme soubor pro cteni
    std::fstream file("vstup.txt", std::ios::in);

    // spusteni algoritmu
    text_statistics(file);
}