#pragma once

// 3 distance functions: Eukleides, Manhattan, Chebyshev

#include "points.h"
#include <cmath>      // std::sqrt, std::abs
#include <algorithm>  // std::max

double EukleideanDistance(const Point2D& p1, const Point2D& p2)
{
    //return std::sqrt( std::pow(p1.getX() - p2.getX(), 2)
    //                + std::pow(p1.getY() - p2.getY(), 2) );
    const double dx = p1.getX() - p2.getX();
    const double dy = p1.getY() - p2.getY();
    return std::sqrt( dx*dx + dy*dy );
}

double ManhattanDistance(const Point2D& p1, const Point2D& p2)
{
    return std::abs(p1.getX() - p2.getX())
         + std::abs(p1.getY() - p2.getY());
}

double ChebyshevDistance(const Point2D& p1, const Point2D& p2)
{
    return std::max(std::abs(p1.getX() - p2.getX()),
                    std::abs(p1.getY() - p2.getY()));
}

double EukleideanDistance(const Point3D& p1, const Point3D& p2)
{
    const double dx = p1.getX() - p2.getX();
    const double dy = p1.getY() - p2.getY();
    const double dz = p1.getZ() - p2.getZ();
    return std::sqrt( dx*dx + dy*dy + dz*dz );
}

double ManhattanDistance(const Point3D& p1, const Point3D& p2)
{
    return std::abs(p1.getX() - p2.getX())
         + std::abs(p1.getY() - p2.getY())
         + std::abs(p1.getZ() - p2.getZ());
}

double ChebyshevDistance(const Point3D& p1, const Point3D& p2)
{
    const double temp = std::max(std::abs(p1.getX() - p2.getX()),
                                 std::abs(p1.getY() - p2.getY()));
    return std::max(temp,
                    std::abs(p1.getZ() - p2.getZ()));
}