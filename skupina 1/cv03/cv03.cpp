// 2 classes: Point2D, Point3D
// 3 distance functions: Eukleides, Manhattan, Chebyshev

#include "points.h"
#include "distance.h"
#include <iostream>

void f2()
{
    Point2D a(1, 1);
    Point2D b(2, 3);

    //double d = a.Eukleides(b);

    double d1 = EukleideanDistance(a, b);
    std::cout << d1 << std::endl;
    double d2 = ManhattanDistance(a, b);
    std::cout << d2 << std::endl;
    double d3 = ChebyshevDistance(a, b);
    std::cout << d3 << std::endl;
}

void f3()
{
    Point3D a(1, 1, 1);
    Point3D b(2, 3, 4);

    //double d = a.Eukleides(b);

    double d1 = EukleideanDistance(a, b);
    std::cout << d1 << std::endl;
    double d2 = ManhattanDistance(a, b);
    std::cout << d2 << std::endl;
    double d3 = ChebyshevDistance(a, b);
    std::cout << d3 << std::endl;
}

int main()
{
    f2();
    f3();
}