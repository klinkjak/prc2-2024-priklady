#ifndef POINTS_H
#define POINTS_H

// 2 classes: Point2D, Point3D

class Point2D
{
private:
    double x;
    double y;

public:
    // konstruktory
    Point2D() : x(0), y(0) {}
    Point2D(double x, double y) : x(x), y(y) {}

    // gettery
    double getX() const { return x; }
    double getY() const { return y; }

    // settery
    void setX(double x) { this->x = x; }
    void setY(double y) { this->y = y; }
};

class Point3D
{
private:
    double x;
    double y;
    double z;

public:
    // konstruktory
    Point3D() : x(0), y(0), z(0) {}
    Point3D(double x, double y, double z) : x(x), y(y), z(z) {}

    // gettery
    double getX() const { return x; }
    double getY() const { return y; }
    double getZ() const { return z; }

    // settery
    void setX(double x) { this->x = x; }
    void setY(double y) { this->y = y; }
    void setZ(double z) { this->z = z; }

    //double Eukleides(Point3D p)
};

#endif