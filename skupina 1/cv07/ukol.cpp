#include "PointCloud.h"
#include "distance.h"
#include "filter.h"

int main()
{
    auto callback = [](const auto& p)
    {
        std::cout << "pridan novy bod" << std::endl;
    };

    constexpr int dim = 3;
    PointCloud<dim, double> cloud(callback);

    std::ifstream data;
    if (dim == 2)
        data.open("data_2d.txt");
    else if (dim == 3)
        data.open("data_3d.txt");
    readPoints(cloud, data);

    auto metrika = EukleideanDistance<dim, double, double>;
    Point<dim, double> p(1.0, 2.0, 3.0);
    double r = 3.0;
    
    // Úkol 1: filtrování podle vzdálenosti od zadaného bodu
    // input: množina bodů (PointCloud), bod p, vzdálenost r, metrika pro výpočet vzdálenosti
    // output: podmnožina bodů ze vstupního PointCloudu

    // 1. přístup - "prototypování"
    // celý algoritmus by se implementoval zde ve funkci main

    // 2. přístup - metoda ve třídě PointCloud
    //auto result = cloud.filter(p, r, metrika);

    // 3. přístup - samostatná funkce
    // Tento přístup je lepší než 2. - splňuje principy SRP a OCP, viz SOLID:
    // https://en.wikipedia.org/wiki/SOLID
    auto result = filter(cloud, p, r, metrika);


    
    // Úkol 2: filtrování podle vzdálenosti od zadaného bodu
    // input: množina bodů (PointCloud), bod p, vzdálenosti r1, r2,
    // metrika pro výpočet vzdálenosti
    // output: podmnožina bodů ze vstupního PointCloudu, které splňují
    //         r1 <= dist <= r2
    auto result2 = filter(cloud, p, r, 2*r, metrika);

    

    
    // Úkol 3: filtrování podle vzdálenosti od zadaného bodu
    // input: množina bodů (PointCloud), bod p, vzdálenosti r1, r2,
    // metrika pro výpočet vzdálenosti
    // output: podmnožina bodů ze vstupního PointCloudu, které splňují
    //         dist <= r1 OR r2 <= dist
    auto result3 = filterOutsideInterval(cloud, p, r, 2*r, metrika);


    // Obecný postup: definovat specifikace pro filtrování
    // Dobrovolný úkol přes Velikonoce: promyslete, jak implementovat
    // "FilterSpecification" a jak předat parametr funkci "filter".
    FilterSpecification spec(p, r, metrika);
    auto result4 = filter(cloud, spec);
}
