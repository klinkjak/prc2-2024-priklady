#pragma once

#include "PointCloud.h"

// Úkol 1: filtrování podle vzdálenosti od zadaného bodu
template <int dim, typename T, typename MetricFunction>
PointCloud<dim, T>
filter(const PointCloud<dim, T>& cloud,
       const Point<dim, T>& point,
       T threshold,
       MetricFunction dist)
{
    PointCloud<dim, T> result;

    for (auto p : cloud)  // funguje díky cloud.begin / end
    {
        auto d = dist(p, point);
        if (d <= threshold)
            result.addPoint(p);
    }

    return result;
}

// Úkol 2: filtrování podle vzdálenosti od zadaného bodu
//         v zadaném intervalu ("mezikruží", "mezikoulí")
template <int dim, typename T, typename MetricFunction>
PointCloud<dim, T>
filter(const PointCloud<dim, T>& cloud,
       const Point<dim, T>& point,
       T threshold_min,
       T threshold_max,
       MetricFunction dist)
{
    PointCloud<dim, T> result;

    for (auto p : cloud)  // funguje díky cloud.begin / end
    {
        auto d = dist(p, point);
        if (threshold_min <= d && d <= threshold_max)
            result.addPoint(p);
    }

    return result;
}

// Úkol 3: filtrování podle vzdálenosti od zadaného bodu
//         mimo zadaný interval
template <int dim, typename T, typename MetricFunction>
PointCloud<dim, T>
filterOutsideInterval(const PointCloud<dim, T>& cloud,
                      const Point<dim, T>& point,
                      T threshold_min,
                      T threshold_max,
                      MetricFunction dist)
{
    PointCloud<dim, T> result;

    for (auto p : cloud)  // funguje díky cloud.begin / end
    {
        auto d = dist(p, point);
        if (threshold_min >= d || d >= threshold_max)
            result.addPoint(p);
    }

    return result;
}


// Obecný návrh
template <int dim, typename T>
PointCloud<dim, T>
filterOutsideInterval(const PointCloud<dim, T>& cloud,
                      ... specification)
{
    PointCloud<dim, T> result;

    for (auto p : cloud)  // funguje díky cloud.begin / end
    {
        // kontrola specifikace pro aktuální bod
        if (specification.is_satisfied(p))
            result.addPoint(p);
    }

    return result;
}