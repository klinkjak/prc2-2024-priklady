#ifndef POINTS_H
#define POINTS_H

#include <ostream>

// obecná deklarace šablony
template <int D, typename T>
class Point
{
private:
    // pole pro všechny souřadnice
    T data[D];

public:
    // konstruktory
    Point()
    {
        for (int i = 0; i < D; i++)
            data[i] = 0;
    }

    Point(T x)
    {
        static_assert(D == 1, "tento konstruktor vyzaduje D == 1");
        //if (D == 1)
            data[0] = x;
        //else
        //    throw std::logic_error("nastala chyba");
    }

    Point(T x, T y)
    {
        static_assert(D == 2, "tento konstruktor vyzaduje D == 2");
        data[0] = x;
        data[1] = y;
    }

    Point(T x, T y, T z)
    {
        static_assert(D == 3, "tento konstruktor vyzaduje D == 3");
        data[0] = x;
        data[1] = y;
        data[2] = z;
    }

    // gettery
    T getX() const
    {
        static_assert(D >= 1);
        return data[0];
    }
    T getY() const 
    {
        static_assert(D >= 1);
        return data[1];
    }
    T getZ() const 
    {
        static_assert(D >= 1);
        return data[2]; 
    }

    // settery
    void setX(T x) 
    { 
        static_assert(D >= 1);
        data[0] = x; 
    }
    void setY(T y) 
    { 
        static_assert(D >= 1);
        data[1] = y; 
    }
    void setZ(T z) 
    { 
        static_assert(D >= 1);
        data[2] = z; 
    }

    // operátor pro přístup k souřadnicím
    T& operator[](int i)
    {
        return data[i];
    }
    
    const T& operator[](int i) const
    {
        return data[i];
    }

    // operátor potřebný pro std::set  (result = (*this < other) )
    bool operator<(const Point<D, T>& other) const
    {
        // lexicografické porovnání
        // TODO
        if (getX() < other.getX()) return true;
        if (other.getX() < getX()) return false;
        return getY() < other.getY();
    }

    int getDimension() const
    {
        return D;
    }
};

// přetížení operátoru pro:  (std::cout << "point = ") << p << std::endl;
template <int D, typename T>
std::ostream& operator<<(std::ostream& stream, const Point<D, T>& p)
{
    stream << "[ ";
    for (int i = 0; i < D; i++) {
        if (i > 0)
            stream << ", ";
        stream << p[i];
    }
    stream << " ]";
    return stream;
}

#endif