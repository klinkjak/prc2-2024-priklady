#include <iostream>

namespace pixel {
    struct point
    {
        int x = 0;
        int y = 0;
    };

    void draw(point p)
    {
        std::cout << "pixel " << p.x << "," << p.y << std::endl;
    }
}

namespace geom {
    struct point
    {
        double x = 0;
        double y = 0;
    };

    void draw(point p)
    {
        std::cout << "geom " << p.x << "," << p.y << std::endl;
    }
}


int main()
{
    pixel::point p;
    geom::point g;

    // explicitní použití
    pixel::draw(p);
    geom::draw(g);

    // implicitní použití: ADL (argument-dependent lookup)
    // https://hackingcpp.com/cpp/lang/adl.html
    draw(p);
    draw(g);
}
