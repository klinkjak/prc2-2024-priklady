// How can a base class use a code defined in a derived class?

#if 0
// Solution 1: using dynamic polymorphism
class Base
{
public:
    void interface()
    {
        implementation();
    }

// NOTE: implementation can be private
private:
    virtual void implementation() = 0;
};

class Derived : public Base
{
private:
    void implementation() override
    {
        // do something...
    }
};

#else
// Solution 2: using static polymorphism
template <typename T>
class Base
{
public:
    void interface()
    {
        T* self = static_cast<T*>(this);
        self->implementation();
    }

    // NOTE: additionally, it is possible to call static methods on T
    static void static_interface()
    {
        // ...
        T::static_implementation();
        // ...
    }
};

class Derived : public Base<Derived>
{
// NOTE: implementation must be public
public:
    void implementation()
    {
        // do something...
    }

    static void static_sub_func()
    {
        // do something...
    }
};
#endif

int main()
{
    Derived obj;
    obj.interface();
}