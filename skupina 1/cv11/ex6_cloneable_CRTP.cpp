#include <memory>

class Interface
{
public:
    virtual ~Interface() = default;
    virtual void doSomething() const = 0;
};

// separate base class for "cloneable" objects
template <typename Derived>
class cloneable
{
public:
    virtual ~cloneable() = default;

    // NOTE: now clone() makes Derived objects!
    std::unique_ptr<Derived> clone() const
    {
        return std::unique_ptr<Derived>(static_cast<Derived*>(this->clone_impl()));
    }

private:
    virtual cloneable* clone_impl() const = 0;
};

// NOTE: A inherits from cloneable<A> (CRTP)
// NOTE: alternatively, we could make CloneableInterface<T> which inherits from Interface and cloneable<T>
class A : public Interface, public cloneable<A>
{
public:
    void doSomething() const override
    {
        // do some work here...
    }

private:
    virtual A* clone_impl() const override
    {
        return new A(*this);
    }
};

int main()
{
    // make an object
    A obj;
    obj.doSomething();

    // make a copy
    auto copy = obj.clone();

    // now this works - copy has type std::unique_ptr<A>
    copy->doSomething();
}