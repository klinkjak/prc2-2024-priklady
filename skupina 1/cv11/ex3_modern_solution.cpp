#include <memory>

class Interface
{
public:
    virtual ~Interface() = default;
    virtual void doSomething() const = 0;

    // new method in the interface - enforces using the RAII pattern
    virtual std::unique_ptr<Interface> clone() const = 0;
};

class A : public Interface
{
public:
    void doSomething() const override
    {
        // do some work here...
    }

    // PROBLEM: covariance does not apply to objects, so we must return std::unique_ptr<Interface>
    std::unique_ptr<Interface> clone() const override
    {
        return std::unique_ptr<A>(new A(*this));
    }
};

void f(const A& x)
{
    // here it is easy to make a copy
    A y = x;

    // we can also use the clone() method
    auto z = x.clone();
    // PROBLEM: ...but then we are limited to the base class interface
}

void g(const Interface& x)
{
    // here we can make a copy using the clone() method
    std::unique_ptr<Interface> y = x.clone();

    // do some work here...
}

int main()
{
    A obj;
    f(obj);
    g(obj);
}