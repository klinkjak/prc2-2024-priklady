class Creature
{
public:
    virtual void draw() = 0;
}

class Bird : public Creature
{
public:
    virtual void setLocation(double longitude, double latitude) = 0;
    virtual bool canFly() const
    {
        return false;
    }
};

class FlyingBird : public Bird
{
public:
    virtual void setAltitude(double altitude) = 0;
    virtual bool canFly() const
    {
        return true;
    }
}

class Parrot : public FlyingBird
{
private:
    double longitude = 0;
    double latitude = 0;
    double altitude = 0;

public:
    virtual void setLocation(double longitude, double latitude)
    {
        this->longitude = longitude;
        this->latitude = latitude;
    }

    virtual void setAltitude(double altitude)
    {
        this->altitude = altitude;
    }

    virtual void draw()
    {
        // zde by byla nějaká užitečná implementace (nejspíš pomocí nějaké grafické knihovny)...
    }
};

class Penguin : public Bird
{
private:
    double longitude = 0;
    double latitude = 0;

public:
    virtual void setLocation(double longitude, double latitude)
    {
        this->longitude = longitude;
        this->latitude = latitude;
    }

    /*
    virtual void setAltitude(double altitude)
    {
        // Penguin does not fly - what to do here???
        //throw std::logic_error("Penguins don't fly!!");
    }
    */

    virtual void draw()
    {
        // zde by byla nějaká užitečná implementace (nejspíš pomocí nějaké grafické knihovny)...
    }
};



/*
void adjustPosition(Bird& bird)
{
    // jak se rozhodnout, jestli pták umí létat?
    Penguin* p = dynamic_cast<Penguin*>(&bird);
    if (p == nullptr) {
        // je to Parrot
    }
    else {
        // je to Penguin
    }

    // dynamic_cast s referencemi:
    try {
        Penguin& p2 = dynamic_cast<Penguin&>(bird);
        // je to Penguin
    }
    catch (std::bad_cast& exception) {
        // je to Parrot
    }
}
*/

void adjustPosition(Bird& bird)
{
    // implementace pro nelétavé ptáky
}

void adjustPosition(FlyingBird& bird)
{
    // implementace pro létavé ptáky
}