#ifndef POINTS_H
#define POINTS_H

// obecná deklarace šablony
template <int D, typename T>
class Point;

// specializace pro 2D
template <typename T>
class Point<2, T>  // <-- !!!
{
private:
    T x;
    T y;

public:
    // konstruktory
    Point() : x(0), y(0) {}
    Point(T x, T y) : x(x), y(y) {}

    // gettery
    T getX() const { return x; }
    T getY() const { return y; }

    // settery
    void setX(T x) { this->x = x; }
    void setY(T y) { this->y = y; }
};

// úplná specializace
//template <>
//class Point<2, char>
//{
//    ...
//};

// specializace pro 3D
template <typename T>
class Point<3, T>  // <-- !!!
{
private:
    T x;
    T y;
    T z;

public:
    // konstruktory
    Point() : x(0), y(0), z(0) {}
    Point(T x, T y, T z) : x(x), y(y), z(z) {}

    // gettery
    T getX() const { return x; }
    T getY() const { return y; }
    T getZ() const { return z; }

    // settery
    void setX(T x) { this->x = x; }
    void setY(T y) { this->y = y; }
    void setZ(T z) { this->z = z; }
};

#endif