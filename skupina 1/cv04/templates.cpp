// "klasické" řešení
#if 0
int min(int a, int b)
{
    return (a < b) ? a : b;
}

long min(long a, long b)
{
    return (a < b) ? a : b;
}

float min(float a, float b)
{
    return (a < b) ? a : b;
}

double min(double a, double b)
{
    return (a < b) ? a : b;
}
#endif


#include <iostream>


// obecné řešení
template <typename T>
const T& min(const T& a, const T& b)
{
    std::cout << "varianta pro hodnoty" << std::endl;
    return (a < b) ? a : b;
}

template <typename T>
const T* min(const T* a, const T* b)
{
    std::cout << "varianta pro pointery" << std::endl;
    // dereference pointeru
    return (*a < *b) ? a : b;
}

// variadic template
template <typename T, typename... Ts>
T min(T a1, Ts... args)
{
    /* možné použití 'args':
        1. sizeof...(args) -> počet parametrů v seznamu
        2. f(args...) -> předání parametrů jiné funkci
    */

    // rekurze
    return min(a1, min(args...));
}


int main()
{
    int m1 = min(10, -100);
    double m2 = min(10.0, 20.0);
    char m3 = min('a', 'z');
    const char* m4 = min("a", "z");

    std::cout << m4 << std::endl;
    std::cout << min(5, 7, 23, 50, -70) << std::endl;
}