#pragma once

// 3 distance functions: Eukleides, Manhattan, Chebyshev

#include "points.h"
#include <cmath>      // std::sqrt, std::abs
#include <algorithm>  // std::max

/*
template <typename T1, typename T2>
double EukleideanDistance(const Point<2, T1>& p1, const Point<2, T2>& p2)
{
    //return std::sqrt( std::pow(p1.getX() - p2.getX(), 2)
    //                + std::pow(p1.getY() - p2.getY(), 2) );
    const auto dx = p1.getX() - p2.getX();
    const auto dy = p1.getY() - p2.getY();
    return std::sqrt( dx*dx + dy*dy );
}
*/

template <typename T1, typename T2>
auto ManhattanDistance(const Point<2, T1>& p1, const Point<2, T2>& p2)
{
    return std::abs(p1.getX() - p2.getX())
         + std::abs(p1.getY() - p2.getY());
}

template <typename T1, typename T2>
auto ChebyshevDistance(const Point<2, T1>& p1, const Point<2, T2>& p2)
{
    return std::max(std::abs(p1.getX() - p2.getX()),
                    std::abs(p1.getY() - p2.getY()));
}

/*
template <typename T1, typename T2>
double EukleideanDistance(const Point<3, T1>& p1, const Point<3, T2>& p2)
{
    const auto dx = p1.getX() - p2.getX();
    const auto dy = p1.getY() - p2.getY();
    const auto dz = p1.getZ() - p2.getZ();
    return std::sqrt( dx*dx + dy*dy + dz*dz );
}
*/

template <typename T1, typename T2>
auto ManhattanDistance(const Point<3, T1>& p1, const Point<3, T2>& p2)
{
    return std::abs(p1.getX() - p2.getX())
         + std::abs(p1.getY() - p2.getY())
         + std::abs(p1.getZ() - p2.getZ());
}

template <typename T1, typename T2>
std::common_type_t<T1, T2> ChebyshevDistance(const Point<3, T1>& p1, const Point<3, T2>& p2)
{
    const auto temp = std::max(std::abs(p1.getX() - p2.getX()),
                               std::abs(p1.getY() - p2.getY()));
    return std::max(temp,
                    std::abs(p1.getZ() - p2.getZ()));
}



// obecná implementace pro všechny dimenze
template <int D, typename T1, typename T2>
double EukleideanDistance(const Point<D, T1>& p1, const Point<D, T2>& p2)
{
    if (D == 2) {
        const auto dx = p1.getX() - p2.getX();
        const auto dy = p1.getY() - p2.getY();
        return std::sqrt( dx*dx + dy*dy );
    }
    else if (D == 3) {
        const auto dx = p1.getX() - p2.getX();
        const auto dy = p1.getY() - p2.getY();
        const auto dz = p1.getZ() - p2.getZ();   // tohle nefunguje pro D == 2
        return std::sqrt( dx*dx + dy*dy + dz*dz );
    }
}