#include "tuple.h"
#include <string>
#include <iostream>

int main()
{
    my::tuple<int> t1;
    my::tuple<int, std::string> t2;
    my::tuple<int, std::string, double> t3;

    
    my::tuple<int, std::string, double> t4(1, "Hello world", 2.3);
    std::cout << "( " << my::get<0>(t4)
              << ", " << my::get<1>(t4)
              << ", " << my::get<2>(t4)
              << " )\n";


    // pouziti tuple_size
    constexpr std::size_t velikost = my::tuple_size< my::tuple<int, std::string, double> >::value;
}


/*

Urovne pri dedeni:

- my::tuple<int, std::string, double>
- my::detail::storage<int, std::string, double>
  (atribut "int value")
- my::detail::storage<std::string, double>
  (atribut "std::string value")
- my::detail::storage<double>  (Tail is empty)
  (atribut "double value")
- my::detail::storage<>   (ukonceni rekurze)

*/