#pragma once

#include <cstddef>

namespace my {

// pomocna struktura pro implementaci tuple
namespace detail {

// dopredna deklarace
template< std::size_t I, typename... Types >
class storage;

// obecna specializace (I je index Head v celem tuple)
template< std::size_t I, typename Head, typename... Tail >
class storage<I, Head, Tail...>
// rekurzivni dedeni
: public storage<I + 1, Tail...>
{
private:
    Head value;

public:
    storage() = default;
    storage(const storage&) = default;
    storage(storage&&) = default;
    storage& operator=(const storage&) = default;
    storage& operator=(storage&&) = default;

    // konstruktor s parametry
    storage(Head arg0, Tail... args)
    : value(arg0), detail::storage<I + 1, Tail...>(args...)
    {}

    template< std::size_t Index >
    auto get()
    {
        if constexpr (Index == I)
            return value;
        else
            return storage<I + 1, Tail...>::template get<Index>();
    }
};

// specializace pro ukonceni rekurze
template<std::size_t I>
class storage<I>
{};

} // namespace detail

template< typename... Types >
class tuple
: public detail::storage<0, Types...>
{
public:
    tuple() = default;
    tuple(const tuple&) = default;
    tuple(tuple&&) = default;
    tuple& operator=(const tuple&) = default;
    tuple& operator=(tuple&&) = default;

    // konstruktor s parametry
    tuple(Types... args) : detail::storage<0, Types...>(args...) {}

    template< std::size_t Index >
    auto get()
    {
        return detail::storage<0, Types...>::template get<Index>();
    }
};

// 2 zpusoby pouziti Types:
// 1. expandovani:  Types...
// 2. sizeof...(Types)


// pomocna trida pro urceni velikosti tuple
template< typename T >
struct tuple_size;

template< typename... Types >
struct tuple_size< tuple<Types...> >
{
    static constexpr std::size_t value = sizeof...(Types);
};


// pomocna trida pro urceni I-teho typu
template< std::size_t Index, typename T >
struct tuple_element;

template< std::size_t Index, typename... Types >
struct tuple_element< Index, tuple<Types...> >
{
    using type = ......;
}


template< std::size_t Index, typename... Types >
//typename tuple_element<Index, tuple<Types...> >::type&
auto
get( tuple<Types...>& t )
{
    return t.template get<Index>();
}

} // namespace my