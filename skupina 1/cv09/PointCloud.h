#pragma once

#include <functional>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <string>

#include "points.h"

template <int dim, typename T>
class PointCloud
{
public:
    using Point = ::Point<dim, T>;
    using CallbackFunction = std::function<void(Point)>;

    // defaultní konstruktor
    PointCloud() = default;

    // konstruktor s callback funkcí (volá se při přidání nového bodu)
    PointCloud(CallbackFunction callback)
    : callback(callback)
    {}

    int getDimension() const
    {
        return dim;
    }

    void addPoint(const Point& p)
    {
        points.insert(p);
        if (callback)
            callback(p);
    }

    // metoda pro nalezení dvou nejvzdálenějších bodů (úkoly 1 a 2)
    // MetricFunction - typ funkce pro měření vzdálenosti; použití: d = dist(p1, p2);
    template <typename MetricFunction>
    std::pair<Point, Point> findMostDistantPoints(MetricFunction dist) const
    {
        T max_distance = -1;
        Point a;
        Point b;

        //for (auto p1 = points.begin(); p1 != points.end(); p1++)
        for (auto p1 : points) {
            for (auto p2 : points) {
                T d = dist(p1, p2);
                if (max_distance < d) {
                    max_distance = d;
                    a = p1;
                    b = p2;
                }
            }
        }

        //return std::make_pair(a, b);
        return {a, b};
    }

    auto begin() const
    {
        return points.begin();
    }

    auto end() const
    {
        return points.end();
    }

private:
    CallbackFunction callback = nullptr;
    std::set<Point> points;
};

template <typename Cloud>
void readPoints(Cloud& cloud, std::istream& str)
{
    while (true) {
        std::string line;
        std::getline(str, line);
        if (!str)
            break;

        std::stringstream s(line);
        typename Cloud::Point p;
        //for (auto& component : p)
        //    s >> component;
        for (int i = 0; i < p.getDimension(); i++)
            s >> p[i];

        if (s.eof() && !s.fail())
            cloud.addPoint(p);
        else
            std::cerr << "chyba pri cteni dat z radku \"" << line << "\"" << std::endl;
    }
}
