#pragma once

#include "PointCloud.h"

// Úkol 1: filtrování podle vzdálenosti od zadaného bodu
// input: množina bodů (PointCloud), bod p, vzdálenost r, metrika pro výpočet vzdálenosti
// output: podmnožina bodů ze vstupního PointCloudu
template <typename Point, typename MetricFunction>
class DistanceFromPointSpecification
{
    Point center;
    double dist;
    MetricFunction metrika;

public:
    DistanceFromPointSpecification(const Point& center,
                                   double dist,
                                   MetricFunction metrika)
    : center(center), dist(dist), metrika(metrika)
    {}

    bool operator()(const Point& p)
    {
        return metrika(p, center) <= dist;
    }
};


// Úkol 2: filtrování podle vzdálenosti od zadaného bodu
// input: množina bodů (PointCloud), bod p, vzdálenosti r1, r2,
// metrika pro výpočet vzdálenosti
// output: podmnožina bodů ze vstupního PointCloudu, které splňují
//         r1 <= dist <= r2
template <typename Point, typename MetricFunction>
class AnnulusSpecification
{
    Point center;
    double r1;
    double r2;
    MetricFunction metrika;

public:
    AnnulusSpecification(const Point& center,
                         double r1,
                         double r2,
                         MetricFunction metrika)
    : center(center), r1(r1), r2(r2), metrika(metrika)
    {}

    bool operator()(const Point& p)
    {
        auto dist = metrika(p, center);
        return r1 <= dist && dist <= r2;
    }
};


// Úkol 3: filtrování podle vzdálenosti od zadaného bodu
// input: množina bodů (PointCloud), bod p, vzdálenosti r1, r2,
// metrika pro výpočet vzdálenosti
// output: podmnožina bodů ze vstupního PointCloudu, které splňují
//         dist <= r1 OR r2 <= dist
template <typename Point, typename MetricFunction>
class OutsideAnnulusSpecification
{
    Point center;
    double r1;
    double r2;
    MetricFunction metrika;

public:
    OutsideAnnulusSpecification(const Point& center,
                                double r1,
                                double r2,
                                MetricFunction metrika)
    : center(center), r1(r1), r2(r2), metrika(metrika)
    {}

    bool operator()(const Point& p)
    {
        auto dist = metrika(p, center);
        return r1 >= dist || dist >= r2;
    }
};


// Obecná funkce pro filtrování bodů - specification je přímo nějaká lambda funkce
template <int dim, typename T, typename Specification>
PointCloud<dim, T>
filter(const PointCloud<dim, T>& cloud,
       Specification specification)
{
    PointCloud<dim, T> result;

    for (auto p : cloud)  // funguje díky cloud.begin / end
    {
        // kontrola specifikace pro aktuální bod
        if (specification(p))
            result.addPoint(p);
    }

    return result;
}