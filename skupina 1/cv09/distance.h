#pragma once

// 3 distance functions: Eukleides, Manhattan, Chebyshev

#include "points.h"
#include <cmath>      // std::sqrt, std::abs
#include <algorithm>  // std::max

template <int D, typename T1, typename T2>
double EukleideanDistance(const Point<D, T1>& p1, const Point<D, T2>& p2)
{
    std::common_type_t<T1, T2> sum = 0;
    for (int i = 0; i < D; i++) {
        auto diff = p1[i] - p2[i];
        sum += diff * diff;
    }
    return std::sqrt(sum);
}

template <int D, typename T1, typename T2>
std::common_type_t<T1, T2> ManhattanDistance(const Point<D, T1>& p1, const Point<D, T2>& p2)
{
    std::common_type_t<T1, T2> sum = 0;
    for (int i = 0; i < D; i++) {
        auto diff = p1[i] - p2[i];
        sum += std::abs(diff);
    }
    return sum;
}

template <int D, typename T1, typename T2>
std::common_type_t<T1, T2> ChebyshevDistance(const Point<D, T1>& p1, const Point<D, T2>& p2)
{
    std::common_type_t<T1, T2> result = 0;
    for (int i = 0; i < D; i++) {
        auto diff = p1[i] - p2[i];
        result = std::max(result, std::abs(diff));
    }
    return result;
}