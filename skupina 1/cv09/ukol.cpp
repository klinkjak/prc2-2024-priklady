#include "PointCloud.h"
#include "distance.h"
#include "filter.h"

int main()
{
    auto callback = [](const auto& p)
    {
        std::cout << "pridan novy bod" << std::endl;
    };

    constexpr int dim = 3;
    PointCloud<dim, double> cloud(callback);

    std::ifstream data;
    if (dim == 2)
        data.open("data_2d.txt");
    else if (dim == 3)
        data.open("data_3d.txt");
    readPoints(cloud, data);

    auto metrika = EukleideanDistance<dim, double, double>;
    Point<dim, double> p(1.0, 2.0, 3.0);
    double r = 3.0;
    
    // Úkol 1: filtrování podle vzdálenosti od zadaného bodu
    // input: množina bodů (PointCloud), bod p, vzdálenost r, metrika pro výpočet vzdálenosti
    // output: podmnožina bodů ze vstupního PointCloudu
    using MetricFunction = decltype(metrika);  // získání typu objektu metrika
    //DistanceFromPointSpecification<Point<dim, double>, MetricFunction> spec(p, r, metrika);
    // definice specifikace pomocí lambda funkce
    auto spec = [&metrika, &r, &p] (const Point<dim, double>& point) -> bool {
        return metrika(point, p) <= r;
    };
    auto result = filter(cloud, spec);

    
    // Úkol 2: filtrování podle vzdálenosti od zadaného bodu
    // input: množina bodů (PointCloud), bod p, vzdálenosti r1, r2,
    // metrika pro výpočet vzdálenosti
    // output: podmnožina bodů ze vstupního PointCloudu, které splňují
    //         r1 <= dist <= r2
    //AnnulusSpecification<Point<dim, double>, MetricFunction> spec2(p, r, 2*r, metrika);
    double r1 = r;
    double r2 = 2 * r;
    auto spec2 = [&metrika, &r1, &r2, &p] (const Point<dim, double>& point) -> bool {
        auto dist = metrika(point, p);
        return r1 <= dist && dist <= r2;
    };
    auto result2 = filter(cloud, spec2);

    

    
    // Úkol 3: filtrování podle vzdálenosti od zadaného bodu
    // input: množina bodů (PointCloud), bod p, vzdálenosti r1, r2,
    // metrika pro výpočet vzdálenosti
    // output: podmnožina bodů ze vstupního PointCloudu, které splňují
    //         dist <= r1 OR r2 <= dist
    //OutsideAnnulusSpecification<Point<dim, double>, MetricFunction> spec3(p, r, 2*r, metrika);
    auto spec3 = [&spec2] (const Point<dim, double>& point) -> bool {
        return ! spec2(point);  // negace spec2
    };
    auto result3 = filter(cloud, spec3);
}
