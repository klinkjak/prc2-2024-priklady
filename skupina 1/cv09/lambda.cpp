#include <iostream>

auto g (int a) -> int
{
    std::cout << "Hello, world!" << std::endl;
    std::cout << "a = " << a << std::endl;
    return 0;
}

template <typename F>
void h(F f)
{
    f();
}

int main()
{
    h([] () {
        std::cout << "Hello, world!" << std::endl;
    });


    int b = 0;
    int c = 0;

    //auto f = [b, &c] (int a) -> int {
    auto f = [&] (int a) -> int {
        std::cout << "Hello, world!" << std::endl;
        a += b + c;
        std::cout << "a = " << a << std::endl;
        return 0;
    };

    f(0);

    b = c = 1;

    f(1);
}