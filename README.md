# PRC2 – příklady ze cvičení

Zde budu postupně přidávat příklady ze cvičení předmětu PRC2.
Kdyby něco dlouho chybělo nebo kdybyste narazili na nějaký problém, tak mě neváhejte oslovit ☺️

- Skupina 1: středa 10:00–11:40
- Skupina 2: středa 16:00–17:40

## Podmínky pro zápočet

[Podmínky pro zápočet](https://jlk.fjfi.cvut.cz/md/s/qQ3B7Uywe)
